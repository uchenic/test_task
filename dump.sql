PRAGMA foreign_keys=OFF;
	BEGIN TRANSACTION;
	CREATE TABLE books (id int primary_key,title varchar(50));
	CREATE TABLE authors (id int primary_key,name varchar(50));
	CREATE TABLE books_authors (author_id int ,book_id int,foreign key(author_id) references authors(id),foreign key(book_id) references books(id) );
	COMMIT;


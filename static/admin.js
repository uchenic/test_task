$().ready(function(){
	$("button[class^='modify']").click(function(ev) {
		var mod=ev.target.className;
		var q = mod.split('-');
		window.location = q.join('/');
	});

	$("button[class^='delete']").click(function(ev) {
		var mod=ev.target.className;
		var q = mod.split('-');
		q[0]='modify';
		$.ajax({
				url:q.join('/'),
				method:'DELETE'
			})
		.done(function  (result) {
				window.location.reload();
			});
		
	});

});

$().ready(function () {
    $('#q').typeahead({
        minLength: 1,
        dynamic: true,
        delay: 500,
        source: {
            books: {
                display: ["title","authors_name"],
                href: "/view/book/{{id}}",
                // data: [{
                //     "id": 415849,
                //     "title": "ddd",
                //     "authors_name": [{"name":"ddfas","id":3}]
                   
                // }],
                template: function  (query,item) {
                    var res ="{{title}}<br/>Authors:{{authors_name}}";
                    // item.authors_name.forEach(function  (argument) {
                    //     res+='<span>'+argument.name+'</span>';
                    // })
                    return res;

                },
                url: [{
                    type: "POST",
                    url: "/search",
                    // contentType:'application/json',
                    data: {
                        q: "{{query}}"
                    },
                    callback: {
                        done: function (data) {
                            //data= JSON.parse(data);
                            
                            return data;
                        }
                    }
                }, "data.books"],
            },
            
        },
        callback: {
            onClickAfter: function (node, a, item, event) {

               
                    window.open(item.href);
              
                $('#result-container').text('');

            },
            onResult: function (node, query, obj, objCount) {

               

                var text = "";
                if (query !== "") {
                    text = objCount + ' elements matching "' + query + '"';
                }
                $('#result-container').text(text);

            }
        }
    });
});


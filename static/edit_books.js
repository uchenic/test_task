$().ready(function(){
	function onAddTag(tag) {
		$.ajax({
				url:'/relate',
				method:'POST',
				contentType:'application/json',
				data:JSON.stringify({title:tag,name:$('.author_name').val()})
			})
		.done(function  (result) {
				//window.location.reload();
			});
	}
	function onRemoveTag(tag) {
		$.ajax({
				url:'/relate',
				method:'DELETE',
				contentType:'application/json',
				data:JSON.stringify({title:tag,name:$('.author_name').val()})
			})
		.done(function  (result) {
				//window.location.reload();
			});
	}

	function onChangeTag(input,tag) {
		alert("Changed a tag: " + tag);
	}
	$.ajax({
				url:'/books/'+$('.author_name').val(),
				method:'GET',
				
				
			})
		.done(function  (result) {
				var res = JSON.parse(result);
				var res_str =[];
				res.forEach(function (argument) {
					res_str.push(argument['value']);
				});
				$('.books-input').val(function  (argument) {
					return res_str.join(',');
				} );
				$('.books-input').tagsInput({
					width: 'auto',
					onAddTag:onAddTag,
					onRemoveTag:onRemoveTag,
					//onChange: onChangeTag,
					
					autocomplete_url:'/books'
				});
			});

	
});
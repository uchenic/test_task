from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Table, ForeignKey, Boolean
from sqlalchemy.orm import relationship

Base = declarative_base()

class Book(Base):
    __tablename__ = 'books'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    authors= relationship("Author", secondary='books_authors')
    
books_authors = Table(
                    'books_authors', Base.metadata,
                    Column('author_id', Integer, ForeignKey('authors.id')),
                    Column('book_id', Integer, ForeignKey('books.id'))
                    )

class Author(Base):
    __tablename__ = 'authors'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    books= relationship("Book", secondary=books_authors)

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    passwd = Column(String)
    admin = Column(Boolean)
    

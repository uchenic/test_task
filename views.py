import hashlib
from flask import Flask, render_template,\
        request, url_for, redirect, abort,make_response, session
from sqlalchemy import func

from forms import AuthorForm, BookForm, LoginForm, SearchForm, SignupForm
from models import Author, Book, User

import config
from utils import create_db_session, login_required, admin_required
import json

app = Flask(__name__)
app.secret_key = config.CSRF_SECRET_KEY 
app.debug = True

@app.route('/')
def hello_world():
    return render_template('main.html')

@app.route('/login',methods=['GET','POST'])
def login():
    aform = LoginForm(request.form)
    if request.method == 'POST':
        if aform.validate():
            user = None
            try:
                sess = create_db_session(config.DB_PATH)
                user = sess.query(User).filter(User.name == aform.login.data).one()
            except Exception:
                return render_template('login_form.html',form=aform)
            digest = hashlib.md5()
            digest.update(aform.passw.data.encode('UTF-8'))
            hashed = digest.hexdigest()

            if hashed == user.passwd:
                session['login']=True
            
                if aform.login.data == 'admin':
                    session['superuser']=True

                    return redirect(url_for('admin'))
            return redirect(url_for('hello_world'))
        return redirect(url_for('hello_world'))
    return render_template('login_form.html',form=aform)

@app.route('/signup',methods=['GET','POST'])
def signup():
    aform = SignupForm(request.form)
    if request.method == 'POST':
        if aform.validate():
            if not aform.passw.data == aform.passw2.data:
                return redirect(url_for('signup'))
            sess = create_db_session(config.DB_PATH)
            user = sess.query(User.id).filter(User.name == aform.username.data).count()
            digest = hashlib.md5()
            digest.update(aform.passw.data.encode('UTF-8'))
            hashed = digest.hexdigest()
            print(user,hashed)
            if user==0:
                a=User(name=aform.username.data,admin=False,passwd=hashed)
           
                if aform.username.data == 'admin':
                    a.admin=True
                sess.add(a)
                sess.commit()
            else:
                return redirect(url_for('signup'))
        else:
            return json.dumps(form.errors),400
        return redirect(url_for('login'))
    return render_template('signup_form.html',form=aform)


@app.route('/logout')
def logout():
    try:
        del session['login']
    except Exception:
        pass
    try:
        del session['superuser']
    except Exception:
        pass
    return redirect(url_for('hello_world'))

@app.route('/admin')
@login_required
@admin_required
def admin():
    sess = create_db_session(config.DB_PATH)
    authors = sess.query(Author).all()
    books = sess.query(Book).all()
    return render_template('admin.html', books=books, authors=authors )

@app.route('/search',methods=['POST','GET'])
@login_required
def search():
    form = SearchForm(request.form)
    if request.method == 'POST':
        #val = json.loads(request.data.decode('UTF-8'))
        query = ''
        if form.validate():
            query = form.q.data
        else:
            return json.dumps(form.errors),400
        #return query
        sess = create_db_session(config.DB_PATH)
        result = sess.query(Book).filter(Book.title.like('%{}%'.format(query))).all()
        for auth in sess.query(Author).filter(Author.name.like('%{}%'.format(query))).all():
            result.extend(auth.books)
        tmp=make_response(json.dumps(
            {'data':
                {'books':list(map(lambda x: {'authors_name':\
            ', '.join(list(map(lambda y:y.name,x.authors))),'title':x.title,'id':x.id},result))}}))
        tmp.mimetype='application/json'
        return tmp
    return render_template('search_form.html')

@app.route('/view/book/<int:id>')
def book_view(id):
    sess = create_db_session(config.DB_PATH)
    result = None
    try:
        result = sess.query(Book).filter(Book.id == id).one()
    except Exception:
        abort(404)
    return render_template('book_view.html',book=result)

@app.route('/books')
@app.route('/books/<string:author_name>')
def books_list(author_name=None):
 
    sess = create_db_session(config.DB_PATH)

    books =[]
    if author_name:
        try:
            author = sess.query(Author).filter(Author.name == author_name).one()
            books = author.books
        except Exception:
            abort(400)
    else:
        books = sess.query(Book).all()
    books = list(map(lambda x: {'author_name':x.id,'label':x.title,'value':x.title},books))

    return json.dumps(books),200

@app.route('/authors')
def authors_list():
 
    sess = create_db_session(config.DB_PATH)

    authors =[]
    authors = sess.query(Author).all()
    authors = list(map(lambda x: {'author_name':x.id,'label':x.name,'value':x.name},authors))

    return json.dumps(authors),200


@app.route('/relate',methods=['POST', 'DELETE'])
@login_required
@admin_required
def relate():
    val = None
    try:
        val = json.loads(request.data.decode('UTF-8'))
    except Exception:
        abort(400)
    if request.method == 'POST':
        try:
            sess = create_db_session(config.DB_PATH)
            author = sess.query(Author).filter(Author.name == val.get('name') ).one()
            book = sess.query(Book).filter(Book.title == val.get('title')).one()
            author.books.append(book)
            sess.commit()
            return 'test'
        except Exception:
            abort(400)
    if request.method == 'DELETE':
        try:
            sess = create_db_session(config.DB_PATH)
            author = sess.query(Author).filter(Author.name == val.get('name') ).one()
            book = sess.query(Book).filter(Book.title == val.get('title')).one()
            author.books.remove(book)
            sess.commit()
            return "will"
        except Exception:
            abort(400)

@app.route('/create/author',methods=['POST', 'GET'])
@login_required
@admin_required
def create_author():
    form = AuthorForm(request.form)
    if request.method == 'POST':
        if form.validate():
            sess = create_db_session(config.DB_PATH)
            author = Author(name= form.name.data)
            sess.add(author)
            sess.commit()
            #return "all is ok",200
            return redirect(url_for('admin'))
        else:
            return json.dumps(form.errors),400
    return render_template('author_form.html',form=form)

@app.route('/modify/author/<int:id>',methods=['POST', 'DELETE', 'GET'])
@login_required
@admin_required
def modify_author(id):
    sess = create_db_session(config.DB_PATH)
    author = None
    try:
        author = sess.query(Author).filter(Author.id == id ).one()
    except Exception :
        abort(404)
    form = AuthorForm(request.form,author)
    if request.method == "POST" :
        if form.validate():
            author.name = form.name.data
            sess.add(author)
            sess.commit()
            #return "all is ok",200
            return redirect(url_for('admin'))

        else:
            return json.dumps(form.errors),400
    elif request.method == "DELETE":
        sess.delete(author)
        sess.commit()
        #return "all is ok",200
        return redirect(url_for('admin'))
    return render_template('author_form.html',form=form )

@app.route('/create/book',methods=['POST', 'GET'])
@login_required
@admin_required
def create_book():
    form = BookForm(request.form)
    if request.method == 'POST':
        if form.validate():
            sess = create_db_session(config.DB_PATH)
            book = Book(title= form.title.data)
            authors = form.authors.data.split(',')
            for author in authors:
                try:
                    q = sess.query(Author).filter(Author.\
                        name.like('%{}%'.format(author))).one()
                    book.authors.append(q)
                except Exception:
                    m = Author(name=author)
                    sess.add(m)
                    book.authors.append(m)
    
            
            sess.add(book)
            sess.commit()
            #return "all is ok",200
            return redirect(url_for('admin'))
        else:
            return json.dumps(form.errors),400
    return render_template('book_form.html',form=form, method='POST')

@app.route('/modify/book/<int:id>',methods=['POST', 'DELETE', 'GET'])
@login_required
@admin_required
def modify_book(id):
    sess = create_db_session(config.DB_PATH)
    book = None
    try:
        book = sess.query(Book).filter(Book.id == id ).one()
    except Exception:
        abort(404)
    form = BookForm(request.form)

    if request.method == "POST" :
        if form.validate():
            book.title = form.title.data
            authors = form.authors.data.split(',')
            for author in authors:
                try:
                    q = sess.query(Author).filter(Author.\
                        name.like('%{}%'.format(author))).one()
                    book.authors.append(q)
                except Exception:
                    m = Author(name=author)
                    sess.add(m)
                    book.authors.append(m)
            for author in book.authors:
                if author.name not in authors:
                    book.authors.remove(author)
  
            sess.add(book)
            sess.commit()
            #return "all is ok",200
            return redirect(url_for('admin'))
        else:
            return json.dumps(form.errors),400
    elif request.method == "DELETE":
        sess.delete(book)
        sess.commit()
        #return "all is ok",200
        return redirect(url_for('admin'))
    form.title.data = book.title
    form.authors.data = ','.join(map(lambda x: x.name,book.authors))
    return render_template('book_form.html',form=form , method='POST')

if __name__ == '__main__':
    app.run()

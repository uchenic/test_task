import re

def string_val(form,inp):
    mtr= re.compile('^[a-zA-Z0-9 ]+')
    return mtr.match(inp.data)

def login_val(form,inp):
    mtr= re.compile('^[a-zA-Z0-9]{4,15}')
    return mtr.match(inp.data)



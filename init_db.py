from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import *

engine = create_engine('sqlite:///db2.sqlite', echo=True)

Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)

session= Session()

author= Author(name="John")

session.add(author)

session.commit()
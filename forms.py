from flask import session
from wtforms.csrf.session import SessionCSRF
from wtforms import Form, StringField, PasswordField, validators
import config
from validators import string_val,login_val


class BaseForm(Form):
    class Meta:
        csrf = True
        csrf_class = SessionCSRF
        csrf_secret =config.CSRF_SECRET_KEY

        @property
        def csrf_context(self):
            return session


class AuthorForm(BaseForm):
	"""docstring for AuthorForm """
	name = StringField('Name', [string_val,validators.Length(min=4, max=25)])

class BookForm(BaseForm):
	"""docstring for BookForm"""
	title = StringField('Title', [string_val,validators.Length(min=4, max=25)])
        authors = StringField('Authors',[validators.Length(min=4,max=100)])
		
class LoginForm(BaseForm):
        login = StringField('Login', [string_val,validators.Length(min=4, max=25)])
        passw = PasswordField('Password')

class SearchForm(Form):
        q = StringField('Search',[string_val])

class SignupForm(BaseForm):
        username = StringField('Username',[login_val,validators.Length(min=4,max=15)])
        passw = PasswordField('Password',[validators.Length(min=8,max=25)])
        passw2 = PasswordField('Password',[validators.Length(min=8,max=25)])


from flask import url_for, redirect, session

from sqlalchemy import create_engine,or_
from sqlalchemy.orm import sessionmaker
from functools import wraps

def create_db_session(path):
    engine = create_engine(path, echo=True)
    Session = sessionmaker(bind=engine)
    session= Session()
    return session

def login_required(fun):
    @wraps(fun)
    def wrapper(*args, **kwargs):
        if 'login' not in session:
            return redirect(url_for('login'))
        return fun(*args, **kwargs)
    return wrapper

def admin_required(fun):
    @wraps(fun)
    def wrapper(*args, **kwargs):
        if 'superuser' not in session:
            return redirect(url_for('login'))
        return fun(*args, **kwargs)
    return wrapper

